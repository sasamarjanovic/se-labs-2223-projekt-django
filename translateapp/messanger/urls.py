from django.urls import path
from . import views

app_name = "messanger"
urlpatterns = [
    path('', views.messanger, name="messanger"),
    path('open_chat/<int:user_id>', views.open_chat, name="open_chat"),
    path('send_mess/<int:user_id>', views.send_mess, name="send_mess"),
]