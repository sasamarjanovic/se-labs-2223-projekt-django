from django.shortcuts import render
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone
from django.urls import reverse
from app.models import Job, Bid, Dispute, Message, Rating
from app.forms import JobForm, BidForm, TranslationForm, DisputeForm, MessageForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

# Create your views here.
@login_required
def messanger(request):
    users_w_m = request.user.account.get_users_with_last_m()
    context = {
        'users':users_w_m,
        'messages': None,
        'send_to':None,
    }
    return render(request, 'messanger/messanger.html', context)

@login_required
def open_chat(request, user_id):
    send_to = get_object_or_404(User, pk=user_id)
    users_w_m = request.user.account.get_users_with_last_m()
    messages = request.user.account.get_messages_for_user(user_id)
    context = {
        'users':users_w_m,
        'messages':messages,
        'send_to':send_to,
        'form':MessageForm()
    }
    return render(request, 'messanger/messanger.html', context)
    
@login_required
def send_mess(request, user_id):
    if request.method == 'POST':
        send_to = get_object_or_404(User, pk=user_id)
        form = MessageForm(request.POST)
        if form.is_valid():
            message = Message(
                sender = request.user,
                receiver = send_to,
                text = form.cleaned_data['text']
            )
            message.save()
            return HttpResponseRedirect(reverse('messanger:open_chat', args=(send_to.id,)))
        else:
            users_w_m = request.user.account.get_users_with_last_m()
            messages = request.user.account.get_messages_for_user(user_id)
            context = {
                'users':users_w_m,
                'messages':messages,
                'send_to':send_to,
                'form':form
            }
            return render(request, 'messanger/messanger.html', context)